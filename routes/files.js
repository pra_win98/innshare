const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const File = require("../models/file");

let storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, "uploads/"),
  filename: (req, file, cb) => {
    const uniqueName = `${Date.now()}-${Math.round(
      Math.random() * 1e9
    )}${path.extname(file.originalname)}`;
    cb(null, uniqueName);
  },
});

let upload = multer({
  storage,
  limit: {
    fileSize: 100000 * 100,
  },
}).single("myfile");

router.post("/", async (req, res) => {
  // Store file
  upload(req, res, async (err) => {
    // Validate request
    if (!req.file) {
      return res.status(400).json({ error: "All fields are required." });
    }
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    // Store into database
    const file = new File({
      filename: req.file.filename,
      uuid: uuidv4(),
      path: req.file.path,
      size: req.file.size,
    });

    const response = await file.save();
    return res.json({
      file: `${process.env.APP_BASE_URL}/files/${response.uuid}`,
    });
    // Response link
  });
});

// Send email
router.post("/send", async (req, res) => {
  const { emailTo, emailFrom, uuid } = req.body;
  // Validate email
  if (!emailTo || !emailFrom || !uuid) {
    return res.status(422).json({ error: "All fields are required." });
  }

  try {
    const file = await File.findOne({ uuid });

    if (file.sender) {
      return res.status(422).json({ error: "Email already sent." });
    }

    file.sender = emailFrom;
    file.reciever = emailTo;

    const response = await file.save();

    const sendEmail = require("../services/email-service");

    sendEmail({
      from: emailFrom,
      to: emailTo,
      subject: "innShare file sharing",
      text: `${emailFrom} shared a file with you.`,
      html: require("../services/email-template")({
        emailFrom,
        downloadLink: `${process.env.APP_BASE_URL}/files/${file.uuid}`,
        size: parseInt(file.size / 1000) + " KB",
        expires: "24 hours",
      }),
    })
      .then(() => {
        return res.status(200).json({ success: true });
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ error: "Error in sending mail." });
      });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Something went wrong!" });
  }
});

module.exports = router;
