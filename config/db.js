const mongoose = require("mongoose");

function connectDb() {
  mongoose.connect(process.env.MONGO_URL, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const connection = mongoose.connection;

  connection
    .once("open", () => console.log(`Database Connected!!`))
    .catch((err) => console.error(`Connection failed.`));
}

module.exports = connectDb;
