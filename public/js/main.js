const dropZone = document.querySelector(".drop-zone");
const fileInput = document.querySelector("#fileInput");
const browseBtn = document.querySelector(".browseBtn");

const progressContainer = document.querySelector(".progress-container");
const bgProgress = document.querySelector(".bg-progress");
const progressBar = document.querySelector(".progress-bar");
const percentText = document.querySelector("#percent");

const fileUrl = document.querySelector("#fileUrl");
const sharingContainer = document.querySelector(".sharing-container");
const copyBtn = document.querySelector("#copyBtn");

const emailForm = document.querySelector("#emailForm");
const toast = document.querySelector(".toast");

const host = window.location.href;
const uploadUrl = `${host}api/files`;
const emailUrl = `${host}api/files/send`;

const maxAllowedSize = 100 * 1024 * 1024;

dropZone.addEventListener("dragover", (e) => {
  e.preventDefault();
  if (!dropZone.classList.contains("dragged")) {
    dropZone.classList.add("dragged");
  }
});

dropZone.addEventListener("dragleave", () => {
  dropZone.classList.remove("dragged");
});

dropZone.addEventListener("drop", (e) => {
  e.preventDefault();
  const files = e.dataTransfer.files;
  if (files.length === 1) {
    if (files[0].size < maxAllowedSize) {
      fileInput.files = files;
      uploadFiles();
    } else {
      showToast("Max file size is 100MB");
    }
  } else if (files.length > 1) {
    showToast("You can't upload multiple files!");
  }
  dropZone.classList.remove("dragged");
});

fileInput.addEventListener("change", () => {
  if (fileInput.files[0].size > maxAllowedSize) {
    showToast("Max file size is 100MB.");
    fileInput.files = "";
    return;
  } else {
    uploadFiles();
  }
});

browseBtn.addEventListener("click", () => {
  fileInput.click();
});

copyBtn.addEventListener("click", () => {
  fileUrl.select();
  document.execCommand("copy");
  showToast("Link copied");
});

function resetFileInput() {
  fileInput.value = "";
}

function uploadFiles() {
  const files = fileInput.files[0];
  var formData = new FormData();
  formData.append("myfile", files);
  progressContainer.style.display = "block";
  const xhr = new XMLHttpRequest();

  xhr.upload.onprogress = updateProgress;

  xhr.upload.onerror = function () {
    showToast(`Error in upload: ${xhr.status}.`);
    fileInput.value = "";
  };

  xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      onUploadSuccess(JSON.parse(xhr.responseText));
    }
  };

  xhr.open("POST", uploadUrl);
  xhr.send(formData);
}

function updateProgress(e) {
  let percent = Math.round((e.loaded / e.total) * 100);
  bgProgress.style.width = `${percent}%`;
  percentText.innerText = percent;
  progressBar.style.transform = `scaleX(${percent / 100})`;
}

function onUploadSuccess({ file }) {
  resetFileInput();
  progressContainer.style.display = "none";
  sharingContainer.style.display = "block";
  emailForm[2].removeAttribute("disabled", "true");
  fileUrl.value = file;
}

emailForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const url = fileUrl.value;
  const formData = {
    uuid: url.split("/").splice(-1, 1)[0],
    emailTo: emailForm.elements["toEmail"].value,
    emailFrom: emailForm.elements["fromEmail"].value,
  };

  emailForm[2].setAttribute("disabled", "true");
  emailForm[2].innerText = "Sending";

  fetch(emailUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(formData),
  })
    .then((res) => res.json())
    .then(({ success }) => {
      if (success) {
        sharingContainer.style.display = "none";
        showToast("Email sent!");
      }
    })
    .catch((err) => {
      console.error(err);
    });
});

let toastTimer;

function showToast(msg) {
  clearTimeout(toastTimer);
  toast.innerText = msg;
  toast.style.transform = "translate(-50%, 0)";
  toastTimer = setTimeout(() => {
    toast.style.transform = "translate(-50%, 60px)";
  }, 2000);
}
